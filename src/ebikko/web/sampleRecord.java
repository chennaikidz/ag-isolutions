package ebikko.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.stringtree.json.JSONValidatingReader;

import ebikko.EbikkoException;
import ebikko.Node;
import ebikko.NodeType;
import ebikko.Repositories;
import ebikko.Repository;
import ebikko.Session;

public class sampleRecord extends HttpServlet {

	private Logger log = Logger.getLogger(sampleRecord.class);
	
	public sampleRecord() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//JSONReader jsonReader = new JSONReader();
		JSONValidatingReader jsonReader = new JSONValidatingReader();
		JSONStringer jsonWriter = new JSONStringer();

		List<String> successList = new ArrayList<String>();
		String responseJSON = "";
		Repository repo = null;
		Session sessionObj = null;
		JSONObject responseMsg = new JSONObject();

		try{
			
			//get session object
			try {
				repo = Repositories.getRepository("mdd");
				log.info("--REPO: " + repo);
				sessionObj = repo.open(repo.getProperty("notif.ebikko.username"), repo.getProperty("notif.ebikko.password"));
				log.info("--SESSION: " + sessionObj);
			} catch (EbikkoException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				responseJSON = "Missing Repository ID or it doesn't exist.";
			}
			
			
			//create record
			//Object jsonObj = jsonReader.read(request.getParameter("json"));
			//HashMap<String, Object> jsonMap = (HashMap<String, Object>) jsonObj;
			StringBuffer jb = new StringBuffer();
			JSONObject jsonObject;
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }

			  
			try {
			    //jsonObject =  HTTP.toJSONObject(jb.toString());
			    jsonObject = new JSONObject(jb.toString());
			    
			  } catch (JSONException e) {
			    // crash and burn
			    throw new IOException("Error parsing JSON request string");
			  }
			
			NodeType newRecord = sessionObj.getNodeTypeByName("Sample Record");
			Node recordNode = new Node(newRecord); 
			//HashMap<String, Object> jsonMap = (HashMap<String, Object>) jsonObject;
			String title = jsonObject.get("title").toString();
			String email = jsonObject.get("email").toString();
			String name = jsonObject.get("name").toString();
			recordNode.setTitle(title);
			recordNode.setValue("Email", email);
			recordNode.setValue("Name", name);
			recordNode.save();
			responseJSON = "Record saved successful";
			jsonWriter.object()
			.key("success").value(false)
			.key("code").value(100)
			.key("data").object()
			.key("responsemsg").value(responseJSON)
			.endObject()
			.endObject();

		}
		catch(Exception ex) 
		{
			log.debug("Save Node Type failed", ex);
			try
			{
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				jsonWriter.object()
				.key("success").value(false)
				.key("code").value(200)
				.key("data").object()
				.key("responsemsg").value("Unable to save node type")
				.endObject()
				.endObject();
			}
			catch(Exception e)
			{
				log.debug("Unable to create status JSON response");
			}
		} 
		finally 
		{
			response.setContentType("application/json");
			response.getWriter().print(jsonWriter.toString());
			response.flushBuffer();
		} 
	}

}
