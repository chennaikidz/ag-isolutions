import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NewRecordComponent } from './new-record/new-record.component';

const routes: Routes = [{
	path: 'sample-form',
	component: NewRecordComponent
},{
	path: '',
	redirectTo: '/',
	pathMatch: 'full'
}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
