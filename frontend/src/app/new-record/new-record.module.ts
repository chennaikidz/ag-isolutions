import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NewRecordComponent } from './new-record.component';

import { NewRecordService } from './new-record.service';

@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, FlexLayoutModule
  ],
  declarations: [
	  NewRecordComponent
  ],
  entryComponents: [
    NewRecordComponent
  ],
  providers: [
    NewRecordService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NewRecordModule { }
