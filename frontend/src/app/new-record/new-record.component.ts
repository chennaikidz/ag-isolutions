import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { trigger, transition, style, group, query, animate, state } from '@angular/animations';
import { NgForm, FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NewRecord } from './new-record';
import { NewRecordService } from './new-record.service';

@Component({
  selector: 'app-new-record',
  templateUrl: './new-record.component.html',
  styleUrls: ['./new-record.component.css']
})
export class NewRecordComponent implements OnInit {
	
  model: NewRecord = new NewRecord();
  showLoad: boolean;   
  constructor(private saveService: NewRecordService) { }

  ngOnInit() {
	this.showLoad = false;
  }
  
  
  save(form: NgForm){
	  this.saveService.saveRecord(this.model).subscribe(response =>{
		  let resp = response;
	  })
  }

}
