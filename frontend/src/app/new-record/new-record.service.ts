import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NewRecord }  from './new-record';

@Injectable({
  providedIn: 'root'
})
export class NewRecordService {

  //private apiUrl = window.location.origin;
  private apiUrl = "http://localhost:8080/ctos-development";

	constructor(private http: HttpClient) { }
  
	saveRecord(newRecord: NewRecord){
      return this.http.post(this.apiUrl + '/CreateRecord', JSON.stringify(newRecord)).pipe(catchError(this.handleError));
  }

  private handleError(error: any): Promise<any> {
     return Promise.reject(error.message || error);
  }

}
